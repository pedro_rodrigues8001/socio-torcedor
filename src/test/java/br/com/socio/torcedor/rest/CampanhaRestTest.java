package br.com.socio.torcedor.rest;

import javax.ws.rs.core.Response;
import static org.junit.Assert.*;
import org.junit.Test;

import br.com.socio.torcedor.enums.Times;

public class CampanhaRestTest {

	@Test
	public void listaCampanhaRest(){
		CampanhaRest rest = new CampanhaRest();
		Response response = rest.listaCampanhasAtivas(Times.CORINTHIANS);
		assertEquals(response.getStatus(), 200);
	}
}
