package br.com.socio.torcedor.interceptor;

import java.io.IOException;
import static org.junit.Assert.*;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.junit.Test;

public class InterceptorTest {

	@Test
	public void restInterceptorTest() throws ClientProtocolException, IOException{
		AppInterceptor app = new AppInterceptor();
		HttpResponse response = app.campanhaServiceIsUp();
		assertEquals(response.getStatusLine().getStatusCode(), 200);
	}
}
