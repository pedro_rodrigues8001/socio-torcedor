package br.com.socio.torcedor.models;

import java.time.LocalDate;
import java.util.UUID;

import static org.junit.Assert.*;
import org.junit.Test;

import br.com.socio.torcedor.enums.Status;

public class CampanhaTest {

	@Test
	public void validaDataVigenteVencida(){
		Campanha campanha = new Campanha(UUID.randomUUID().toString(),"Camiseta", LocalDate.now().minusDays(3));
		campanha.ativa();
		assertEquals(campanha.getStatusCampanha(), Status.EXPIRADO);
		assertEquals(campanha.getUltimaAtualizacao(), LocalDate.now());
	}
}
