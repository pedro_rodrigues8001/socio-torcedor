package br.com.socio.torcedor.models;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.junit.Assert.*;
import org.junit.Test;

import br.com.socio.torcedor.enums.Status;
import br.com.socio.torcedor.enums.Times;

public class UsuarioTest {

	@Test
	public void adicionaCampanhaTest(){
		Usuario usuario = new Usuario("Pedro Rodrigues", "pedro@gmail.com", LocalDate.now().minusYears(24), Times.CORINTHIANS);
		Campanha c1 = new Campanha(UUID.randomUUID().toString(),"Camiseta", LocalDate.now());
		Campanha c2 = new Campanha(UUID.randomUUID().toString(),"Shorts", LocalDate.now().minusDays(5));
		Campanha c3 = new Campanha(UUID.randomUUID().toString(),"Calca", LocalDate.now().plusDays(3));
		
		
		List<Campanha> campanhas = new ArrayList<>();
		campanhas.add(c1);
		campanhas.add(c2);
		
		usuario.getCampanhasAtivas().addAll(campanhas);
		usuario.isAtualiza(c3);
		usuario.isAtualiza(c2);
		
		int index = usuario.getCampanhasAtivas().indexOf(c2);
		
		assertEquals(3, usuario.getCampanhasAtivas().size());
		assertEquals(Status.EXPIRADO, usuario.getCampanhasAtivas().get(index).getStatusCampanha());
		
	}
}
