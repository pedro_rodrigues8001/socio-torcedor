package br.com.socio.torcedor.service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;
import static org.mockito.Mockito.*;

import br.com.socio.torcedor.enums.Status;
import br.com.socio.torcedor.models.Campanha;
import br.com.socio.torcedor.repository.CampanhaRepository;
import br.com.socio.torcedor.rest.CampanhaRest;

public class CampanhaServiceTest {

	private CampanhaRepository repo;
	
	private CampanhaRest rest;
	
	@Before
	public void init(){
		repo = mock(CampanhaRepository.class);
		rest = mock(CampanhaRest.class);
	}
	
	@Test
	public void validaAtualizacaoDasCampanhas(){
		CampanhaService service = new CampanhaService(repo, rest);
		Campanha c1 = new Campanha(UUID.randomUUID().toString(),"Camiseta", LocalDate.now());
		Campanha c2 = new Campanha(UUID.randomUUID().toString(),"Shorts", LocalDate.now().minusDays(5));
		
		List<Campanha> campanhas = new ArrayList<>();
		campanhas.add(c1);
		campanhas.add(c2);
		
		when(repo.save(c1)).thenReturn(c1.ativa());
		when(repo.save(c2)).thenReturn(c2.ativa());
		
		service.atualizaCampanhas(campanhas);
		
		assertEquals(c1.getStatusCampanha(), Status.ATIVO);
		assertEquals(c1.getUltimaAtualizacao(), LocalDate.now());
		
		assertEquals(c2.getStatusCampanha(), Status.EXPIRADO);
		assertEquals(c1.getUltimaAtualizacao(), LocalDate.now());
		
		verify(repo, times(1)).save(c1);
		verify(repo, times(1)).save(c2);
	}
}
