package br.com.socio.torcedor.enums;

import com.google.gson.annotations.SerializedName;

public enum Times {
	
	@SerializedName("corinthians")
	CORINTHIANS("Corinthians"),
	
	@SerializedName("palmeiras")
	PALMEIRAS("Palmeiras"),
	
	@SerializedName("santos")
	SANTOS("Santos"),
	
	@SerializedName("são paulo")
	SAO_PAULO("São Paulo"),
	
	@SerializedName("ponte preta")
	PONTE_PRETA("Ponte Preta");
	
	private String nome;

	Times(String nome) {
		this.nome = nome;
	}
	
	public String nome() {
		return nome;
	}
}
