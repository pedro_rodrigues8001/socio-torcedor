package br.com.socio.torcedor.enums;

public enum Status {

	ATIVO, EXPIRADO;
}
