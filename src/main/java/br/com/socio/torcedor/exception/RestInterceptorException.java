package br.com.socio.torcedor.exception;

public class RestInterceptorException extends RuntimeException {

	private static final long serialVersionUID = -4338243775695691077L;

	public RestInterceptorException(String message) {
		super(message);
	}
	
}
