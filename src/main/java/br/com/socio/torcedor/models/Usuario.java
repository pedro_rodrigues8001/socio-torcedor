package br.com.socio.torcedor.models;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import br.com.socio.torcedor.enums.Times;

@Entity
public class Usuario implements Serializable{

	private static final long serialVersionUID = -5416911984181546226L;

	@Id
	@Expose(serialize = false)
	private String id = UUID.randomUUID().toString();
	
	@Expose
	@NotEmpty(message = "Favor informar o nome")
	private String nome;
	
	@Expose
	@Column(unique = true)
	@NotEmpty(message = "Favor informar o email")
	@Email(message = "Favor informar um e-mail válido")
	private String email;
	
	@Expose
	@Column
	@SerializedName("data_nascimento")
	@NotNull(message = "Favor informar a data de nascimento")
	private LocalDate dataNascimento;
	
	@Expose
	@Enumerated(EnumType.STRING)
	@SerializedName("time_coracao")
	@NotNull(message = "Favor informar o time do coracão")
	private Times timeCoracao;
	
	@Expose
	@SerializedName("campanhas_ativas")
	@ManyToMany(fetch = FetchType.EAGER, cascade = {CascadeType.MERGE})
	private List<Campanha> campanhasAtivas;

	public Usuario() {
	}

	public Usuario(String nome, String email, LocalDate dataNascimento, Times timeCoracao) {
		this.nome = nome;
		this.email = email;
		this.dataNascimento = dataNascimento;
		this.timeCoracao = timeCoracao;
	}

	public String getId() {
		return id;
	}

	public String getNome() {
		return nome;
	}

	public String getEmail() {
		return email;
	}

	public LocalDate getDataNascimento() {
		return dataNascimento;
	}

	public Times getTimeCoracao() {
		return timeCoracao;
	}

	public List<Campanha> getCampanhasAtivas() {
		if(campanhasAtivas == null)
			this.campanhasAtivas = new ArrayList<>();
		return campanhasAtivas;
	}
	
	public boolean isContemCampanhas(){
		return getCampanhasAtivas().isEmpty();
	}
	
	public void atualizaCampanhas(final List<Campanha> campanhasAtualizadas){
		campanhasAtualizadas
			.parallelStream()
			.forEach(c -> isAtualiza(c));
	}

	public void isAtualiza(Campanha c) {
		if(getCampanhasAtivas().contains(c))
			atualiza(c);
		else
			addCampanha(c);
	}

	public void addCampanha(Campanha c) {
		getCampanhasAtivas().add(c.ativa());
	}

	private void atualiza(Campanha c) {
		int indexOf = getCampanhasAtivas().indexOf(c);
		Campanha campanha = getCampanhasAtivas().get(indexOf);
		getCampanhasAtivas().remove(indexOf);
		campanha.atualiza(c);
		addCampanha(campanha);
	}
	
}
