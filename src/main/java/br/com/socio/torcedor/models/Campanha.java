package br.com.socio.torcedor.models;

import java.io.Serializable;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import br.com.socio.torcedor.enums.Status;

@Entity
public class Campanha implements Serializable{

	private static final long serialVersionUID = 8250793942748944188L;

	@Id
	@Expose
	private String id;
	
	@Expose
	private String nome;
	
	@Expose
	@SerializedName("status_campanha")
	@Enumerated(EnumType.STRING)
	private Status statusCampanha = Status.ATIVO;
	
	@Expose
	@Column
	@SerializedName("ultima_atualizacao")
	private LocalDate ultimaAtualizacao;
	
	@Expose
	@Column
	@SerializedName("fim_campanha")
	private LocalDate dataVigente;

	public Campanha() {
	}

	public Campanha(String id,String nome, LocalDate dataVigente) {
		this.id = id;
		this.nome = nome;
		this.dataVigente = dataVigente;
	}

	public String getId() {
		return id;
	}

	public String getNome() {
		return nome;
	}

	public Status getStatusCampanha() {
		return statusCampanha;
	}

	public LocalDate getUltimaAtualizacao() {
		return ultimaAtualizacao;
	}

	public LocalDate getDataVigente() {
		return dataVigente;
	}
	
	public Campanha ativa(){
		this.ultimaAtualizacao = LocalDate.now();
		if(this.dataVigente.compareTo(LocalDate.now()) < 0)
			this.statusCampanha = Status.EXPIRADO;
		return this;
	}
	
	public Campanha atualiza(final Campanha campanhaAtualizada){
		this.dataVigente = campanhaAtualizada.getDataVigente();
		return ativa();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Campanha other = (Campanha) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
}
