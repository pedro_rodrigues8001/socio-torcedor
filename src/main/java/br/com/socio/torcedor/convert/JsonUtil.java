package br.com.socio.torcedor.convert;

import java.time.LocalDate;
import java.time.LocalDateTime;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class JsonUtil {

	private JsonUtil(){
    }

	public static Gson getInstance(){
		return new GsonBuilder().excludeFieldsWithoutExposeAnnotation()
		 				.registerTypeAdapter(LocalDateTime.class, new DateTimeAdapter())
		 				.registerTypeAdapter(LocalDate.class, new DateAdapter())
		 				.setPrettyPrinting()
		 				.create();
	}
	
	public static <T> T asObject(String json, Class<T> clazz) {
		Gson gson = getInstance();
		return gson.fromJson(json, clazz);
	}

	public static <T> String asJson(T object) {
		Gson gson = getInstance();
		return gson.toJson(object);
	}
}
