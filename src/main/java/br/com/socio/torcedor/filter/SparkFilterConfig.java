package br.com.socio.torcedor.filter;

import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import spark.servlet.SparkFilter;

@Component
public class SparkFilterConfig {

	@Bean
	public FilterRegistrationBean someFilterRegistration() {

	    FilterRegistrationBean registration = new FilterRegistrationBean();
	    registration.setFilter(someFilter());
	    registration.addUrlPatterns("/*");
	    registration.addInitParameter("applicationClass", "br.com.socio.torcedor.routes.impl.RoutesImpl");
	    registration.setName("SparkFilter");
	    registration.setOrder(1);
	    return registration;
	} 

	public SparkFilter someFilter() {
	    return new SparkFilter();
	}
}
