package br.com.socio.torcedor;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

import br.com.socio.torcedor.infra.AppConfig;

@SpringBootApplication
@EnableAutoConfiguration
@Import(AppConfig.class)
public class Boot {

	public static void main(String[] args) {
		SpringApplication.run(Boot.class, args);
	}

}
