package br.com.socio.torcedor.repository;

import java.util.List;

import org.springframework.data.repository.Repository;

import br.com.socio.torcedor.enums.Times;
import br.com.socio.torcedor.models.Usuario;

public interface UsuarioRepository extends Repository<Usuario, String>{

	Usuario save(final Usuario usuario);
	
	Usuario findByEmail(final String email);
	
	List<Usuario> findByTimeCoracao(final Times timeCoracao);
	
	List<Usuario> findAll();
}
