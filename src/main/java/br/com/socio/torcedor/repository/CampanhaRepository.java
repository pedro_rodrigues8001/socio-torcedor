package br.com.socio.torcedor.repository;

import org.springframework.data.repository.Repository;

import br.com.socio.torcedor.models.Campanha;

public interface CampanhaRepository extends Repository<Campanha, String>{

	Campanha save(final Campanha campanha);
}
