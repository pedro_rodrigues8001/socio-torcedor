package br.com.socio.torcedor.interceptor;

import java.io.IOException;

import org.apache.http.HttpHeaders;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpOptions;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.HttpClientBuilder;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;

import br.com.socio.torcedor.exception.RestInterceptorException;

@Aspect
public class AppInterceptor {

	private final String url = "http://camp-test.jelasticlw.com.br";
	
	@Before("@annotation(br.com.socio.torcedor.annotations.RestInterceptor)")
	public HttpResponse campanhaServiceIsUp() throws ClientProtocolException, IOException {
			HttpUriRequest request = new HttpOptions(url);
			request.setHeader(HttpHeaders.EXPIRES, "100");
			HttpResponse httpResponse = HttpClientBuilder.create().build().execute( request );
			
			if(httpResponse.getStatusLine().getStatusCode() != 200)
				throw new RestInterceptorException("Servico de campanhas está off");
			
			return httpResponse;
	}
	
	
	
}
