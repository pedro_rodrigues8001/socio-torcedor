package br.com.socio.torcedor.infra;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

import br.com.socio.torcedor.interceptor.AppInterceptor;
import br.com.socio.torcedor.rest.CampanhaRest;

@Configuration
@EnableAspectJAutoProxy
public class AppConfig {

	@Bean
	public AppInterceptor campanhaRestInterceptor(){
		return new AppInterceptor();
	}
	
	@Bean
	public CampanhaRest campanhaRest(){
		return new CampanhaRest();
	}
}
