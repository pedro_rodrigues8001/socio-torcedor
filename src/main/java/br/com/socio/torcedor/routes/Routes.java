package br.com.socio.torcedor.routes;

import spark.servlet.SparkApplication;

public interface Routes extends SparkApplication, Util{

	void filter();
}
