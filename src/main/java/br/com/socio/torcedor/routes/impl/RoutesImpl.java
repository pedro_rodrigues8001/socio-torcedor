package br.com.socio.torcedor.routes.impl;

import br.com.socio.torcedor.enums.Times;
import br.com.socio.torcedor.models.Usuario;
import br.com.socio.torcedor.routes.Routes;
import br.com.socio.torcedor.service.CampanhaService;
import br.com.socio.torcedor.service.UsuarioService;
import spark.Request;

import static spark.Spark.*;

import org.springframework.stereotype.Component;

import static br.com.socio.torcedor.convert.JsonUtil.*;
import static br.com.socio.torcedor.convert.JsonTransformer.*;

@Component
public class RoutesImpl implements Routes {

	private UsuarioService service;

	private CampanhaService campanhaService;

	@Override
	public void init() {

		exeptionHandler();

		config();

		filter();

		path("/socio-torcedor", () -> {

			post("/novo", (req, resp) -> {
				Usuario usuario = getUsuario(req);
				return service.novoCadastro(usuario);
			}, jsonTransformer());

			get("/lista", (req, resp) -> {
				service = getServico(UsuarioService.class);
				return service.listaTodos();
			}, jsonTransformer());

			put("/atualiza/campanha/:time", (req, resp) -> {
				new Thread( () -> {
					campanhaService = getServico(CampanhaService.class);
					Times timeCoracao = getTime(req);
					service.atualizaCampanhaPeloTime(timeCoracao, campanhaService.convertJsonToListCampanhas(req.body()));
				}).start();
				return "";
			}, jsonTransformer());

		});

	}


	@Override
	public void filter() {
		path("/socio-torcedor", () -> {

			before("/novo", (req, resp) -> {
				service = getServico(UsuarioService.class);
				Usuario usuario = getUsuario(req);

				if (service.busca(usuario.getEmail()) != null && service.busca(usuario.getEmail()).isContemCampanhas()) {
					halt(200, asJson(service.buscaCampanhasPeloTime(usuario.getTimeCoracao())));
				}else if(service.busca(usuario.getEmail()) != null){
					halt(200, asJson("Email já está cadastrado"));
				}
			});
			
			before("/atualiza/campanha/:time", (req ,resp) -> {
				service = getServico(UsuarioService.class);
				if(service.listaUsuariosPeloTime(getTime(req)).isEmpty())
					halt(204);
			});
			
			after("/novo", (req, resp) -> {
				new Thread( () -> {
					Usuario usuario = getUsuario(req);
					usuario = service.busca(usuario.getEmail());
					service.adicionaCampanhas(usuario);
				}).start();
			});

		});
	}

	private Usuario getUsuario(Request req) {
		Usuario usuario = asObject(req.body(), Usuario.class);
		return usuario;
	}
	
	private Times getTime(Request req) {
		Times timeCoracao = Times.valueOf(req.params(":time").toUpperCase());
		return timeCoracao;
	}

}
