package br.com.socio.torcedor.service;

import java.io.Serializable;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.socio.torcedor.annotations.RestInterceptor;
import br.com.socio.torcedor.enums.Times;
import br.com.socio.torcedor.models.Campanha;
import br.com.socio.torcedor.models.Usuario;
import br.com.socio.torcedor.repository.UsuarioRepository;

@Service
public class UsuarioService implements Serializable{

	private static final long serialVersionUID = -7470209244770654829L;

	@Autowired
	private UsuarioRepository repository;

	@Autowired
	private CampanhaService service;
	
	public UsuarioService() {
	}
	
	public UsuarioService(UsuarioRepository repository) {
		this.repository = repository;
	}

	public Usuario novoCadastro(final Usuario usuario){
		return save(usuario);
	}
	
	@Transactional
	public Usuario save(final Usuario usuario){
		return repository.save(usuario);
	}

	@Transactional
	public Usuario busca(final String email){
		return repository.findByEmail(email);
	}
	
	@Transactional
	public List<Usuario> listaTodos(){
		return repository.findAll();
	}
	
	@Transactional
	public List<Usuario> listaUsuariosPeloTime(final Times timeCoracao){
		return repository.findByTimeCoracao(timeCoracao);
	}
	
	@RestInterceptor
	public List<Campanha> buscaCampanhasPeloTime(final Times timeCoracao){
		return service.buscaCampanhasAtivasPeloTime(timeCoracao);
	}
	
	@RestInterceptor
	public void adicionaCampanhas(final Usuario usuario) {
		List<Campanha> campanhas = service.buscaCampanhasAtivasPeloTime(usuario.getTimeCoracao());
		usuario.atualizaCampanhas(campanhas);
		save(usuario);
	}
	
	public void atualizaCampanhaPeloTime(final Times timeCoracao, final List<Campanha> campanhaAtualizadas){
		List<Usuario> usuarios = listaUsuariosPeloTime(timeCoracao);
		usuarios.parallelStream().forEach(u -> {
			u.atualizaCampanhas(campanhaAtualizadas);
			save(u);
		});
	}

	
	
}
