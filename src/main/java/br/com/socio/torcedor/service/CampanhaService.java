package br.com.socio.torcedor.service;

import static br.com.socio.torcedor.convert.JsonUtil.getInstance;

import java.io.Serializable;
import java.lang.reflect.Type;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import br.com.socio.torcedor.enums.Times;
import br.com.socio.torcedor.models.Campanha;
import br.com.socio.torcedor.repository.CampanhaRepository;
import br.com.socio.torcedor.rest.CampanhaRest;

@Service
public class CampanhaService implements Serializable{

	private static final long serialVersionUID = 9095349741274656909L;

	@Autowired
	private CampanhaRest rest;
	
	@Autowired
	private CampanhaRepository repository;
	
	public CampanhaService() {
	}

	public CampanhaService(CampanhaRepository repository, CampanhaRest rest) {
		this.repository = repository;
		this.rest = rest;
	}

	@Transactional
	public Campanha save(final Campanha campanha){
		return repository.save(campanha);
	}
	
	public void atualizaCampanhas(final List<Campanha> campanhaAtualizadas) {
		campanhaAtualizadas.parallelStream().forEach(c -> save(c.ativa()));
	}
	
	public List<Campanha> buscaCampanhasAtivasPeloTime(final Times timeCoracao){
		return convertJsonToListCampanhas(rest.listaCampanhasAtivas(timeCoracao).readEntity(String.class));
	}

	public List<Campanha> convertJsonToListCampanhas(final String json) {
		Gson gson = getInstance();
		Type collection = new TypeToken<List<Campanha>>() {}.getType();
		return gson.fromJson(json, collection);
	}
}
