package br.com.socio.torcedor.rest;

import java.io.Serializable;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;

import org.springframework.stereotype.Component;

import br.com.socio.torcedor.enums.Times;

@Component
public class CampanhaRest implements Serializable{

	private static final long serialVersionUID = 6084134391361193345L;

	private static final String url = "http://camp-test.jelasticlw.com.br/campanha/lista/";
	
	public Response listaCampanhasAtivas(final Times timeCoracao){
		Client client = ClientBuilder.newClient();
		WebTarget target = client.target(url);
		return target.path(timeCoracao.nome()).request().get();
	}
	
	
}
