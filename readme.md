# Observações

Está faltando alguns testes no projeto,  em razão do pouco tempo que tive para desenvolve-lo ( a prova foi me passada na sexta-feira - 20.10.17- no final do dia, e por conta de um imprevisto não consegui focar no projeto no final de semana).

Se possível, gostaria de terminar esses testes.

# Arquitetura

Os projetos foram feitos em RestFull com foco em micro-serviço leves.  Foi utilizado Spring Boot e [Spark Framework](http://sparkjava.com/).

## Spring Boot: 
Optei pelo Spring Boot por todo o case de ferramentas que ele possui hoje, apesar de não ter implementado por questão de tempo, acho que se encaixaria muito bem  web-flux neste projeto. Outro motivo por ter optado pelo spring foi pela facilidade de iniciar o projeto e ter um gasto de energia enorme em config e digitacão.

## Spark Framework:
Apesar do spark ser semelhante ao spring boot, quando o assunto é criação de  projeto autoexecutável com foco em RestFull, o spark  consegue ser mais leve que o spring. Só não utilizei apenas o spark por causa da sua deficiência de ferramentas na manutenção de transações.
Assim, optei por usa-lo apenas para a criação das rotas dos serviços, coisa que com o spark se ganha muito tempo,  sem dizer o controle no lifecycle das requisições com filtros antes e pós execução dos requests.

# Exercício 4

Deadlock nada mais é do que a thread aguardando o retorno da thread B, e assim por diante. Para resolver esse problema é importante criar metódos com retorno void para que não fique nenhuma thread aguardando a outra.


# Exercício 5

Basicamente o Stream executa em forma syncrona e o ParallelStreams em modo "Asyncrono".


# Projetos

Os projetos estão hospedados em um servidor, caso queiram testar. A seguir segue as rotas e os formatos dos jsons:


## Rotas - Campanha

path: http://camp-test.jelasticlw.com.br/campanha

nova: /nova

busca: /busca/:id

lista: /lista

lista pelo time: /lista/:time-coracao

## Jsons - Campanha

nova: {
"nome": "Camiseta",
"time_coracao": "Ponte Preta",
"inicio_campanha": "01/10/2017",
"fim_campanha": "30/11/2017"
}

## Rotas - Socio Torcedor

path: http://socio-test.jelasticlw.com.br/socio-torcedor

Novo: /novo

Lista: /lista

Atualiza Campanhas: /atualiza/camapanha/:time

## Jsons

Novo: {
"nome": "Pedro Rodrigues",
"email": "pedro@gmail.com",
"data_nascimento": "05/05/1994",
"time_coracao": "corinthians"
}

Atualiza Campanha: [
    {
        "id": "57361b55-15c6-44f3-95ed-c4b677551ee9",
        "nome": "Camiseta",
        "fim_campanha": "09/11/2017"
    },
    {
        "id": "57361b55-15c6-44f3-95el-c4b677551ee9",
        "nome": "Tenis",
        "fim_campanha": "08/11/2017"
    }
]
##